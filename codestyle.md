# Code style

## General Guielines

### Optimize for readability and predictability.

This is the foundational principle of everything else in this document. Think hard about the names that you give
to concepts, and their structure and function. Names must always strive to be unambiguous and suggestive,
and the structure and function of constructs should be predictable.

### Take advantage of Xcode’s code snippets feature.

Many of the discouraged activities in this document stem from the desire to avoid typing repetitive code.
Use Xcode’s code snippets feature instead of raising the codebase’s vocabulary with new definitions.

### Indent with two (2) spaces.

Two units make indentation less extravagant than four units when nesting scopes. Moreover, code is often 
copied from  the IDE and pasted into an external environment, such as Slack or StackOverflow, which are often
excessive in tab styling. Spaces, however, almost always take up the width of one character in monospaced 
fonts wherever you paste code.

## Inheritance

### Use of the Template Method very, very sparingly.

The Template Method, as described by the Gang of Four in *Design Patterns: Elements of Reusable
Object-Oriented Software* (1994), is a design pattern where a parent class invokes overrideable functions as
it executes an algorithm. Child classes then extend the parent, override the functions, and provide custom logic.

Use this pattern very sparingly and see if your desired logic can be implemented through some other means, 
such as object composition. The Template Method is one of the most-abused patterns in iOS application 
development, where a "god" view controller contains all of the possible variations in behavior that
child view controllers could ever have, with child view controllers only providing values to the parent. This is
a recipe for massive view controllers that are incredibly difficult to read and debug, and it makes it difficult
to anticipate the behavior of child view controllers by looking at the child view controller code alone.

Generally, you should avoid defining abstractions before you know what variations may take form; abstactions
are better off defined after the variations are known. More importantly, you should allow view controllers to exist
as independent entities so that view controllers are free to wildly digress from each other without affecting
each other.

## Extensions

### Avoid non-universal modifications.

If the behavior of an instance or copy of a type is needed only by a limited number of consumers, then the 
behavior should not be in an extension that universally applies the behavior to all instances or copies of the 
type. Use a utility class instead.

## Property Observers

### Property observers should only be used to modify the values of dependent stored properties.

Property observers are often abused to invoke functions within `willSet` or `didSet`, but nobody setting
properties from the outside ever expects that doing so would invoke functions under the hood.

Any process that commences after setting a property must explicitly be called by the client via a function 
instead of being automatically invoked by property observers. Use property observers only to modify other 
stored properties that are dependent on the value of the property being modified.

## Typealiases

### Use typealiases very sparingly.

Typealiases inherently obscure their underlying values and increase the codebase’s vocabulary. Strive to 
use typealiases only where Swift’s protocol system requires it.

## Protocols

### Avoid "handicap" protocols that only serve as compile-time reminders to write repetitive code.

If you are repeating code, use Xcode’s code snippets feature. Remember that protocols are constraints on
types. Do not constrain types if you just want to be reminded about writing a set of code that keeps repeating.
