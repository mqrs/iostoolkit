//
//  LoadableView.swift
//  iOSToolkit
//
//  Created by Matthew Quiros on 4/1/21.
//  Copyright © 2021 Matthew Quiros. All rights reserved.
//

import UIKit

/// A view whose subviews are meant to be shown or hidden depending on the execution state
/// of an operation that fetches the content.
class LoadableView: UIView {
  
  /// The view for when the content is still loading.
  let loadingView: UIView
  /// The view that displays the content.
  let contentView: UIView
  /// The view for when the content fails loading.
  let failureView: UIView
  
  init(loadingView: UIView, contentView: UIView, failureView: UIView) {
    self.loadingView = loadingView
    self.contentView = contentView
    self.failureView = failureView
    super.init(frame: .zero)
    initializeProperties()
    initializeViewHierarchy()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Programmatic View Definition
  
  /// Sets up the stored properties.
  fileprivate func initializeProperties() {
    loadingView.isHidden = true
    contentView.isHidden = true
    failureView.isHidden = true
  }
  
  /// Defines the view hierarchy.
  fileprivate func initializeViewHierarchy() {
    addSubview(loadingView)
    addSubview(contentView)
    addSubview(failureView)
  }
  
}
