//
//  ImageFetchOperation.swift
//  iOSToolkit
//
//  Created by Matthew Quiros on 4/1/21.
//  Copyright © 2021 Matthew Quiros. All rights reserved.
//

import Foundation

/// An operation for fetching a remote image.
class ImageFetchOperation: AsyncOperation<Data, Swift.Error> {
  
  enum Error: Swift.Error {
    /// The supplied image URL is not a valid URL.
    case invalidURL
    /// The server returned successfully but without data.
    case returnedWithoutData
  }
  
  /// The URL string for the image's location.
  let urlString: String
  /// The `URLSession` from which the data task will be based.
  let session: URLSession
  
  fileprivate(set) var dataTask: URLSessionDataTask?
  
  /// Creates an instance of the operation.
  /// - Parameters:
  ///   - urlString: The string representation of the image's URL.
  ///   - session: The `URLSession` from which the data task will be based.
  ///   - completionBlock: The operation's completion block.
  init(urlString: String, session: URLSession, completionBlock: OperationCompletionBlock?) {
    self.urlString = urlString
    self.session = session
    super.init(completionBlock: completionBlock)
  }
  
  override func main() {
    guard let url = URL(string: urlString)
    else {
      result = .failure(Error.invalidURL)
      finish()
      return
    }
    let request = URLRequest(url: url)
    dataTask = session.dataTask(with: request) { (data, response, error) in
      defer {
        self.finish()
      }
      if let error = error {
        self.result = .failure(error)
        return
      }
      if let data = data {
        self.result = .success(data)
      } else {
        self.result = .failure(Error.returnedWithoutData)
      }
    }
    dataTask?.resume()
  }
  
}
