//
//  Operation.swift
//  iOSToolkit
//
//  Created by Matthew Quiros on 25/06/2019.
//  Copyright © 2019 Matthew Quiros. All rights reserved.
//

import Foundation

/// A synchronous operation that produces a result. If the operation is successful, the result
/// holds a value of type `SuccessType`. Otherwise, the result holds a value of type `FailureType`.
///
/// This class is meant to be extended and not to be used as-is. It is up to the developer to
/// supply the types and to update the `result` property accordingly as the operation progresses
/// in its execution.
///
/// When the operation is finished, it is typical to want to access the value of `result`.
/// There are two ways to do so.
///
/// First, you can access the result in the operation's completion block:
///
///    let operation = SomeCustomOperation() { (op) in
///      guard op.isCancelled == false,
///        let result = op.result
///        else {
///          return
///      }
///      DispatchQueue.main.async {
///        switch result {
///        case .success(let successResult):
///          // ... do something
///        case .failure(let error):
///          // ... do something
///        }
///      }
///    }
///
///  Another way to access the result is after the fact of an operation's synchronous execution:
///
///    let operation = SomeCustomOperation(completionBlock: nil)
///    operationQueue.addOperations([operation], waitUntilFinished: true)
///    guard let result = operation.result
///      else {
///        return
///    }
///    switch result {
///    case .success(let successResult):
///      // ... do something
///    case .failure(let error):
///      // ... do something
///    }
///
class Operation<SuccessType, FailureType: Error>: Foundation.Operation {
  
  /// The type produced by this operation if it succeeds.
  typealias SuccessType = SuccessType
  
  /// The type produced by this operation if it fails.
  typealias FailureType = FailureType
  
  /// The type of the result produced by this operation.
  typealias Result = Swift.Result<SuccessType, FailureType>
  
  /// The result produced by the operation. You are expected to modify this value in your
  /// subclass as you go along your operation's execution.
  ///
  /// The default value is `nil`. Note that a `nil` result applies to both cancelled and ready
  /// operations. Do not equate the nullity of this value to the operation's execution state.
  var result: Result?
  
  // Internal flags to track operation state.
  private var internalReady = true
  private var internalExecuting = false
  private var internalFinished = false
  
  override var isReady: Bool {
    return internalReady && super.isReady
  }
  
  override var isExecuting: Bool {
    return internalExecuting
  }
  
  override var isFinished: Bool {
    return internalFinished
  }
  
  typealias OperationCompletionBlock = (_ operation: Operation<SuccessType, FailureType>) -> Void
  
  init(completionBlock: OperationCompletionBlock?) {
    super.init()
    
    // Gives the caller easy access to cancellation state and result.
    self.completionBlock = {
      completionBlock?(self)
    }
  }
  
  override func start() {
    setReady(false)
    if isCancelled == true {
      setFinished(true)
      return
    }
    setExecuting(true)
    main()
    finish()
  }
  
  /// Sets the `isReady` property and makes the corresponding KVO calls.
  func setReady(_ ready: Bool) {
    willChangeValue(forKey: "isReady")
    internalReady = ready
    didChangeValue(forKey: "isReady")
  }
  
  /// Sets the `isExecuting` property and makes the corresponding KVO calls.
  func setExecuting(_ executing: Bool) {
    willChangeValue(forKey: "isExecuting")
    internalExecuting = executing
    didChangeValue(forKey: "isExecuting")
  }
  
  /// Sets the `isFinished` property and makes the corresponding KVO calls.
  func setFinished(_ finished: Bool) {
    willChangeValue(forKey: "isFinished")
    internalFinished = finished
    didChangeValue(forKey: "isFinished")
  }
  
  /// Convenience function for setting the execution state to `false`
  /// and the finished state to `true`.
  func finish() {
    setExecuting(false)
    setFinished(true)
  }
  
}
