//
//  ProgrammaticViewDefinition.swift
//  iOSToolkit
//
//  Created by Matthew Quiros on 4/1/21.
//  Copyright © 2021 Matthew Quiros. All rights reserved.
//

import UIKit

/// Code snippet for programmatically defining views.

class SomeView: UIView {
  
  override init(frame: CGRect) {
    // Instantiate properties here.
    super.init(frame: frame)
    initializeProperties()
    initializeViewHierarchy()
    initializeAutoLayoutRules()
    initializeStyle()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Programmatic View Definition
  
  /// Sets up the stored properties.
  fileprivate func initializeProperties() {
    // This function is meant to contain value assignments to properties that have nothing
    // to do with style and Auto Layout. For example:
    
    // isOpaque = false
    // titleLabel.numberOfLines = 0
    // imageView.contentMode = .scaleAspectFill
  }
  
  /// Defines the view hierarchy.
  fileprivate func initializeViewHierarchy() {
    // This function is intented to contain only calls to `UIView.addSubview()` or
    // `UIStackView.addArrangedSubview()`.
    
    // Moreover, this function must be called absolutely before `initializeAutoLayoutRules()`.
    // It is a fatal error to do otherwise, since you cannot add Auto Layout to views that have
    // no common ancestor.
  }
  
  /// Creates and activates the initial Auto Layout rules.
  fileprivate func initializeAutoLayoutRules() {
    // This function is intended to contain only the creation and activation of Auto Layout rules.
    // You must call this function only after `initializeViewHierarchy()`. Doing otherwise is
    // a fatal error, since you cannot add Auto Layout to views that have no common ancestor.
  }
  
  /// Applies initial styling to the view hierarchy.
  fileprivate func initializeStyle() {
    // This function is intended to contain only initial styling code. "Style" refers to fonts,
    // colors, and dimensions.
    
    // Note that this function should only contain initial styling code. Views and its subviews
    // may take on a different style when the view is in a different state.
  }
  
}
